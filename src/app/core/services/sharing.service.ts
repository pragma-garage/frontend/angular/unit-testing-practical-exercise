import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SharingService {
  private informacionSubject: BehaviorSubject<string[]> = new BehaviorSubject([
    'HOLA',
  ]);

  getInformacion() {
    return this.informacionSubject.asObservable();
  }

  setInformacion(informacion: string[]) {
    this.informacionSubject.next(informacion);
  }
}
