import { Component } from '@angular/core';
import { SharingService } from 'src/app/core/services/sharing.service';

@Component({
  selector: 'app-lazy',
  templateUrl: './lazy.component.html',
  styleUrls: ['./lazy.component.scss'],
})
export class LazyComponent {
  variableInterna: any[] = [];
  subject: any;

  constructor(private sharingService: SharingService) {
    this.subject = this.sharingService.getInformacion();
  }

  suma(num1: number, num2: number) {
    return num1 + num2;
  }
}
